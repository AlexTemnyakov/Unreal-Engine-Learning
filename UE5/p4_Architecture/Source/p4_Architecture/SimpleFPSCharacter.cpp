// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleFPSCharacter.h"

// Sets default values
ASimpleFPSCharacter::ASimpleFPSCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a first person camera component.
	FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	check(FPSCameraComponent != nullptr);

	// Attach the camera component to our capsule component.
	FPSCameraComponent->SetupAttachment(CastChecked<USceneComponent, UCapsuleComponent>(GetCapsuleComponent()));

	// Position the camera slightly above the eyes.
	FPSCameraComponent->SetRelativeLocation(FVector(-100.0f, 0.0f, 50.0f + BaseEyeHeight));
	FPSCameraComponent->SetRelativeRotation(FRotator(-30.0f, 0.0f, 0.0f));

	// Enable the pawn to control camera rotation.
	FPSCameraComponent->bUsePawnControlRotation = true;

	// The owning player doesn't see the regular (third-person) body mesh.
	GetMesh()->SetOwnerNoSee(true);

	TriggerCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger Capsule"));
	TriggerCapsule->InitCapsuleSize(55.f, 96.0f);;
	TriggerCapsule->SetCollisionProfileName(TEXT("Trigger"));
	TriggerCapsule->SetupAttachment(RootComponent);

	//TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &ASimpleFPSCharacter::OnOverlapBegin);
	//TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &ASimpleFPSCharacter::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ASimpleFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("We are using FPSCharacter."));

	TriggerCapsule->OnComponentBeginOverlap.AddDynamic(this, &ASimpleFPSCharacter::OnOverlapBegin);
	TriggerCapsule->OnComponentEndOverlap.AddDynamic(this, &ASimpleFPSCharacter::OnOverlapEnd);
}

// Called every frame
void ASimpleFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASimpleFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up "movement" bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &ASimpleFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASimpleFPSCharacter::MoveRight);

	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &ASimpleFPSCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &ASimpleFPSCharacter::AddControllerPitchInput);
}

void ASimpleFPSCharacter::MoveForward(float Value)
{
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void ASimpleFPSCharacter::MoveRight(float Value)
{
	// Find out which way is "right" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

void ASimpleFPSCharacter::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// check if Actors do not equal nullptr
	if (OtherActor && (OtherActor != this))
	{
		//UE_LOG(LogTemp, Warning, TEXT("OnOverlapBegin"));
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("OnOverlapBegin"));
	}
}

void ASimpleFPSCharacter::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this))
	{
		//UE_LOG(LogTemp, Warning, TEXT("OnOverlapEnd"));
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("OnOverlapEnd"));
	}
}
